//
//  ProductsTableViewController.swift
//  AppleProducts
//
//  Created by Yijia Huang on 9/10/17.
//  Copyright © 2017 Yijia Huang. All rights reserved.
//

// * Custom Table View Cell

// 1 - Design the new cell in Storyboard -- x
// 2 - Create a subclass of UITableViewCell for the new cell -- x
// 3 - Update cell with UITableViewDataSource

// --

// * Delete Rows
// 1. Edit butiion on the right
// 2. Delete a row (in our dasta model && in out tableView -- UI)
// 3. Nice animation - move the table view rows up


// --

// *  Move rows around table view
// 1. tell the table view that you want to be able to move rows aroudn
// 2. update the data model & update the table view UI


// -- 
// *  Design the product detail screen using static Table View
// 1. Create a custom class for the new table view
// 2. Get the data model for the class and populate data into the table view
// 3. Click on the photo, display the camera/ photos library
// 4. Transition from the productsTVC to product detail TVC
// 5. Update the dedit of a product

import UIKit

class ProductsTableViewController: UITableViewController {

    

    @IBOutlet weak var menuBarButton: UIBarButtonItem!
    // MARK: - Data model
    
    var productLines: [ProductLine] = ProductLine.getProductLines()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        menuBarButton.action = #selector(SidebarViewController.toggleLeftMenu(animated:))
            
        tableView.estimatedRowHeight = tableView.rowHeight
        tableView.rowHeight = UITableViewAutomaticDimension
        
        navigationItem.rightBarButtonItem = editButtonItem
    }

    // MARK: - UITableViewDataSource
    
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return productLines.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productLines[section].products.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell", for: indexPath) as! ProductTableViewCell
        let productLine = productLines[indexPath.section]
        let products = productLine.products
        let product = products[indexPath.row]
        cell.product = product
        return cell
    }
    
    // Multiple Sections
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let productLine = productLines[section]
        return productLine.name
    }
    
    // Delete Rows
    
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // 1 - delete the product from the productLine's products array
            let productLine = productLines[indexPath.section]
            productLine.products.remove(at: indexPath.row)
            
            // 2 - update the table view with new data
            // tableView.reloadData() // bad way
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }

    }
    
    // Moving Cells 
    
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
       
        let productLine = productLines[sourceIndexPath.section]
        var products = productLine.products
        let productToMove = products[sourceIndexPath.row]

        // move productToMove to destination products
        productLines[destinationIndexPath.section].products.insert(productToMove, at: destinationIndexPath.row)
        
        // delete the productToMove from the source products
        products.remove(at: sourceIndexPath.row)
    }
    
    // MARK: - UITableViewDelegate
    
    var selectedProduct: Product?
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let productLine = productLines[indexPath.section]
        let product = productLine.products[indexPath.row]
        
        selectedProduct = product
        
        performSegue(withIdentifier: "ShowProductDetail", sender: nil)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowProductDetail" {
            if segue.identifier == "ShowProductDetail" {
                let productDetailTVC = segue.destination as! ProductDetailTableViewController
                productDetailTVC.product = selectedProduct
            }
        }
    }
    
}

















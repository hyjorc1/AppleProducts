//
//  AppDelegate.swift
//  AppleProducts
//
//  Created by Yijia Huang on 9/10/17.
//  Copyright © 2017 Yijia Huang. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    var menuNav: UINavigationController!
    var sidebarVC: SidebarViewController!
    var productsNav: UINavigationController!
    
    var productViewControllers = [ProductsTableViewController]()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        for productLine in ProductLine.getProductLines() {
            let productsTVC = storyboard.instantiateViewController(withIdentifier: "ProductsTVC") as! ProductsTableViewController
            productsTVC.productLines = [productLine]
            productViewControllers.append(productsTVC)
        }
        
        productsNav = UINavigationController(rootViewController: productViewControllers[0])
        
        let menuVC = storyboard.instantiateViewController(withIdentifier: "MenuVC") as! MenuTableViewController
        menuVC.delegate = self
        menuNav = UINavigationController(rootViewController: menuVC)
        
        sidebarVC = SidebarViewController(leftViewController: menuVC, mainViewController: productsNav, overlap: 86.0)
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = sidebarVC
        window?.makeKeyAndVisible()
        
        
        return true
    }


}

extension AppDelegate : MenuTableViewControllerDelegate {
    func menuTableViewController(_ controller: MenuTableViewController, didSelectRow row: Int) {
        let selectedVC = productViewControllers[row]
        if productsNav.topViewController != selectedVC {
            productsNav.setViewControllers([selectedVC], animated: true)
        }
    }
}


//
//  MenuTableViewController.swift
//  AppleProducts
//
//  Created by Yijia Huang on 9/10/17.
//  Copyright © 2017 Yijia Huang. All rights reserved.
//

import UIKit

protocol MenuTableViewControllerDelegate: class {
    func menuTableViewController(_ controller: MenuTableViewController, didSelectRow row: Int)
}

class MenuTableViewController: UITableViewController
{
    weak var delegate: MenuTableViewControllerDelegate?
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        delegate?.menuTableViewController(self, didSelectRow: indexPath.row)
    }
}

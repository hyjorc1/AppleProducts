//
//  SidebarViewController.swift
//  AppleProducts
//
//  Created by Yijia Huang on 9/10/17.
//  Copyright © 2017 Yijia Huang. All rights reserved.
//

import UIKit

class  SidebarViewController: UIViewController {
    
    var leftViewController : UIViewController! // the menu
    var mainViewController : UIViewController! // your main UI view
    var overlap: CGFloat!
    
    var scrollView: UIScrollView!
    var isFirstOpen = true // first open
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if isFirstOpen {
            isFirstOpen = false
            closeMenu(animated: false)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        assert(false, "Use init(leftViewController: mainViewController: overlap:) for this sidebarVC instead")
        super.init(coder: aDecoder)!
    }
    
    init(leftViewController: UIViewController, mainViewController: UIViewController, overlap: CGFloat) {
        self.leftViewController = leftViewController
        self.mainViewController = mainViewController
        self.overlap = overlap
        
        super.init(nibName: nil, bundle: nil)
        
        self.view.backgroundColor = .white
        self.setupScrollView()
        self.setupViewControllers()
    }
    
    func setupScrollView() {
        scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.bounces = false
        scrollView.isPagingEnabled = true
        scrollView.showsVerticalScrollIndicator = false
        
        view.addSubview(scrollView)
        
        // add auto layout constraints for the scrollview
        let horizntalConstrainst = NSLayoutConstraint.constraints(withVisualFormat: "H:|[scrollView]|", options: [], metrics: nil, views: ["scrollView" : scrollView])
        let verticalConstraints =  NSLayoutConstraint.constraints(withVisualFormat: "V:|[scrollView]|", options: [], metrics: nil, views: ["scrollView" : scrollView])
        NSLayoutConstraint.activate(horizntalConstrainst + verticalConstraints)
    }
    
    private func setupViewControllers()
    {
        addViewController(viewController: leftViewController)
        addViewController(viewController: mainViewController)
        
        // apply auto layout to lefVC and mainVC's views
        let views: [String : UIView] = [
            "left" : leftViewController.view,
            "main" : mainViewController.view,
            "outer" : view
        ]
        
        let horizontalContraints = NSLayoutConstraint.constraints(withVisualFormat: "|[left][main(==outer)]|", options: [.alignAllTop, .alignAllBottom], metrics: nil, views: views)
        let leftWidthConstraint = NSLayoutConstraint(item: leftViewController.view, attribute: .width, relatedBy: .equal, toItem: view, attribute: .width, multiplier: 1.0, constant: -overlap)
        let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|[main(==outer)]|", options: [], metrics: nil, views: views)
        
        mainViewController.view.addShadow()
        
        NSLayoutConstraint.activate(horizontalContraints + verticalConstraints + [leftWidthConstraint])
    }
    
    private func addViewController(viewController: UIViewController) {
        viewController.view.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(viewController.view)
        
        self.addChildViewController(viewController)
        viewController.didMove(toParentViewController: self)
    }
    
    
    // MARK: - Close and open the menu bar
    
    func toggleLeftMenu(animated: Bool)
    {
        if isLeftMenuOpen() {
            closeMenu(animated: animated)
        } else {
            openLeftMenu(animated: animated)
        }
    }
    
    func closeMenu(animated: Bool)
    {
        scrollView.setContentOffset(CGPoint(x: leftViewController.view.frame.width, y: 0), animated: animated)
    }
    
    func openLeftMenu(animated: Bool) {
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: animated)
    }
    
    func isLeftMenuOpen() -> Bool {
        return scrollView.contentOffset.x == 0
    }
    
    
    // CC1: close the menu when a menu item is selected
    // CC2: click the menu button, open the sidebar menu
}

private extension UIView
{
    func addShadow()
    {
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shadowRadius = 2.5
        layer.shadowOffset = CGSize(width: 0, height: 0)
        layer.shadowOpacity = 0.7
        layer.shadowColor = UIColor.black.cgColor
    }
}
































